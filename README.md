# Cleverbot

Running on:
* elixir 1.8.1
* erlang 21.0

Grant exec privileges:
* `chmod +x run.sh`

To start the Cleverbot:

* `docker-compose up`


Go into [`front-end app`](https://gitlab.com/guipdutra/cleverbot-front) and start using `yarn start`

Now you can visit [`localhost:4001`](http://localhost:4001) from your browser.
